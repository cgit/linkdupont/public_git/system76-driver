%define		debug_package %{nil}
%define		modulename system76driver

Name:		system76-driver
Version:	17.10.8
Release:	1%{?dist}
Summary:	Universal driver for System76 computers

License:	GPLv2
URL:		https://github.com/pop-os/system76-driver
Source0:	https://github.com/pop-os/system76-driver/archive/%{version}.tar.gz#/%{name}-%{version}.tar.gz
Source1:	http://linkdupont.fedorapeople.org/sources/system76.service

Patch0:		system76-driver-15.10.12-desktop-version.patch
Patch1:		system76-driver-15.10.12-update-grub.patch

BuildArch:	noarch
BuildRequires:	python3-devel >= 3.4
Requires:	dmidecode
Requires:	system76-wallpapers >= 17.04.0
BuildRequires:	systemd

%description
System76 Driver provides drivers, restore, and regression support for System76
computers.


%post
systemctl enable system76.service
systemctl start system76.service


%preun
systemctl stop system76.service
systemctl disable system76.service


%prep
%autosetup -p0


%build
%{__python3} setup.py build


%install
%{__python3} setup.py install --prefix=%{_prefix} --root=%{buildroot}
install -m755 -D system76-daemon %{buildroot}%{_bindir}/system76-daemon
install -m644 -D %{SOURCE1} %{buildroot}%{_unitdir}/system76.service
install -m755 -D system76-driver-pkexec %{buildroot}%{_bindir}/system76-driver-pkexec
install -m644 -D com.system76.pkexec.system76-driver.policy %{buildroot}%{_datadir}/polkit-1/actions/com.system76.pkexec.system76-driver.policy
install -m755 -d %{buildroot}%{_sharedstatedir}/%{name}


%files
%license LICENSE
%doc README
%{_bindir}/%{name}*
%{_bindir}/system76-daemon
%{_unitdir}/system76.service
%{python3_sitelib}/%{modulename}*
%{_datadir}/applications/%{name}.desktop
%{_datadir}/icons/hicolor/scalable/apps/%{name}.svg
%{_datadir}/polkit-1/actions/com.system76.pkexec.system76-driver.policy
%{_sharedstatedir}/%{name}


%changelog
* Sun Nov 26 2017 Link Dupont - 17.10.8-1
- New upstream release

* Sat Jul 1 2017 Link Dupont - 17.04.6-1
- New upstream release

* Fri Apr 7 2017 Link Dupont - 17.04.0-1
- New upstream release

* Mon Mar 27 2017 Link Dupont - 16.10.18-1
- New upstream release

* Thu Feb 2 2017 Link Dupont - 16.10.11-1
- New upstream release

* Thu Sep 22 2016 Link Dupont - 16.10.1-1
- New upstream release

* Mon Sep 12 2016 Link Dupont - 16.04.9-1
- New upstream release

* Fri Jun 17 2016 Link Dupont - 16.04.3-1
- New upstream release

* Fri Apr 22 2016 Link Dupont - 16.04.2-1
- New upstream release

* Wed Feb 24 2016 Link Dupont - 15.10.14-1
- New upstream release

* Tue Feb  9 2016 Link Dupont - 15.10.13-1
- New upstream release
- Add patch to correctly rebuild grub.cfg on distributions without 'update-grub' (namely Ubuntu)

* Sat Dec 26 2015 Link Dupont - 15.10.12-1
- Initial package
